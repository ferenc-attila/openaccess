#!/bin/bash
set -e

TABLES='bnpi_fajlista fajlista'

for TABLE in $TABLES
do

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" < /docker-entrypoint-initdb.d/sql/${TABLE}.sql
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    copy $TABLE from '/docker-entrypoint-initdb.d/lists/${TABLE}.csv' delimiter ',' csv header;
EOSQL

done

